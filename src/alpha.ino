

/*
GPRS SMS Read


There are two methods to read SMS:
  1. GPRS_LoopHandle.ino  -> in order to recieve "+CMTI: "SM"" 
      may be you need to send this command to your shield: "AT+CNMI=2,2,0,0,0"
  2. GPRS_SMSread.ino -> you have to check if there are any 
      UNREAD sms, and you don't need to check serial data continuosly

create on 2015/05/14, version: 1.0
by op2op2op2(op2op2op2@hotmail.com)
*/

#include <GPRS_Shield_Arduino.h>
#include <SoftwareSerial.h>
#include <Wire.h>
#include <LiquidCrystal.h>

#define PIN_TX    7
#define PIN_RX    8
#define BAUDRATE  9600

#define MESSAGE_LENGTH 160  //standard SMS length
char message[MESSAGE_LENGTH]; //incoming message
int messageIndex = 0; //index of incoming message read
const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);
char phone[16]; //phone number of received SMS, Universal
char datetime[24];  //Date-Time String, like "DD/MM/YY,HH:MM:SS+08"
char serialBuffer[10];  //Buffer used for saving incoming data, used for diagnostics and control
char temp[100];//general purpose for saving sim900 data return
String tempS;//general purpose String
String s; //message String, used for removing \0 char form initial message
int signalStrength; //stores the value of signal 0-30
float vcc=0; //max value 4241 so percentage goes (temp/4241)*100

//Custom LCD Symbols
byte antennaSymbol[] = {
  B11111,
  B10101,
  B10101,
  B01110,
  B00100,
  B00100,
  B00100,
  B00100
};
byte marginal[] = {
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B01000,
  B11000
};
byte ok[] = {
  B00000,
  B00000,
  B00000,
  B00000,
  B00100,
  B00100,
  B01100,
  B11100
};
byte good[] = {
  B00000,
  B00000,
  B00010,
  B00010,
  B00110,
  B00110,
  B01110,
  B11110
};
byte Excellent[] = {
  B00001,
  B00001,
  B00011,
  B00011,
  B00111,
  B00111,
  B01111,
  B11111
};


/*
//sms variables for diagnostics
String dataSms; //has all variables as a string
bool net; //network variable to sms
bool sim; //sim status variable to sms
String D; // has the words day-time
String T; //  has the date and the time
String wordSig; //signal word example "good"
int valueSig; // signal number example 15
*/

GPRS gprs(PIN_TX,PIN_RX,BAUDRATE);//RX,TX,PWR,BaudRate


void setup() {
  lcd.begin(20,4);
  lcd.createChar(0, antennaSymbol);
  lcd.createChar(1, marginal);
  lcd.createChar(2, ok);
  lcd.createChar(3, good);
  lcd.createChar(4, Excellent);
  gprs.checkPowerUp();  //Power State of module, 0 off, 1 on
  Serial.begin(9600);
  Serial.setTimeout(200);
  //Module initiallization, including SIM card check & signal strength, 1 for connected, 0 otherwise
  while(!gprs.init()) {
      Serial.print("init error\r\n");
      delay(1000);
  }
  delay(3000);  
  Serial.println("Init Success, please send SMS message to me!");
  pinMode(13, OUTPUT);  //for testing purposes only     
  
}

void loop() {
   smsCheck();
   serialHandler();
   lcdPrintInfo();   
}

void serialHandler(){
  if(Serial.available()){
    Serial.readBytes(serialBuffer, 10);
    Serial.print("Command Received: ");
    Serial.println(serialBuffer);
    diag();
  }
}

//When called checks for incoming, unread, SMS messages
//Then calls handler

void smsCheck(){
  messageIndex = gprs.isSMSunread();
   if (messageIndex > 0) { //At least, there is one UNREAD SMS
      gprs.readSMS(messageIndex, message, MESSAGE_LENGTH, phone, datetime);
      //In order not to full SIM Memory, is better to delete it
      gprs.deleteSMS(messageIndex);
      Serial.print("From number: ");
      Serial.println(phone);  
      Serial.print("Datetime: ");
      Serial.println(datetime);        
      Serial.print("Recieved Message: ");
      Serial.println(message);
      handler(); //HANDLER    
   }
}

//Called when a new message is received, acts according to message content
void handler(){
  s="";
  Serial.println("This is handler talking");
  for(int i=0; i<MESSAGE_LENGTH; i++){
    if(!message[i]=='\0')
    s+=message[i];
  }
  if( s == "start" || s == "Start"){
  Serial.println("Hello");
  digitalWrite(13, HIGH);
  gprs.sendSMS(phone," to led anapse");
  }
  if( s == "stop" || s == "Stop"){
  Serial.println("goodbye");
  digitalWrite(13, LOW);
  gprs.sendSMS(phone," to led esvise");
  }
  if ( s == "diag" || s == "Diag"){
   Serial.println("Diagnostics");
   gprs.sendSMS(phone,"diagnostika bgikan sto monitor");
   diag();
   //smsDiag();
  }
}



void diag(){
  Serial.println("-------------------------");

  //Network Registration
  Serial.print("Network Registration: ");
  Serial.println(gprs.isNetworkRegistered()); //1-Connect to network

  //SIM Status
  Serial.print("SIM Status: ");
  Serial.println(gprs.checkSIMStatus());  //1-SIM inserted

  //Subscriber Number NOT WORKING AND PROBABLY NEVER WILL
  if(gprs.getSubscriberNumber(temp) == 1){
    Serial.print("Number: ");
    Serial.println(temp);
  }else{
    Serial.println("Error on reading SIM number");
    /* if the fuction doesnt work run these AT commands
     *  Serial.write("AT+CPBS='ON'\r");
       Serial.write("ATCPBW=1+{+306987426575},145\r");
       Serial.write("AT+CPBS='SM'\r"); */
  }

  //Date-Time
  if(gprs.getDateTime(temp) == 1){
    Serial.print("Date-Time: ");
    Serial.println(temp); //YY/MM/DD,HH:MM:SS+08
  }else{
    Serial.println("Error on reading Date-Time");
  }

  //Signal Strength
  if(gprs.getSignalStrength(&signalStrength) == 1){
    Serial.print("Signal strength:  ");
    if(signalStrength >=2 && signalStrength <=9){
      Serial.print("Marginal: ");
      Serial.println(signalStrength);
    }else if(signalStrength >=10 && signalStrength <=14){
      Serial.print("OK: ");
      Serial.println(signalStrength);
    }else if(signalStrength >=15 && signalStrength <=19){
      Serial.print("Good: ");
      Serial.println(signalStrength);
    }else if(signalStrength >=20 && signalStrength <=30){
      Serial.print("Excellent: ");
      Serial.println(signalStrength);
    }
  }else{
    Serial.println("Error on reading Signal Strength");
  }

  //Battery voltage, NOT WORKING
  if(gprs.getVcc(temp)==1){
    Serial.print("Battery Percentage: ");
    vcc=atof(temp);
    Serial.print(((vcc/4241)*100));
    Serial.println("%");
  }else{
    Serial.println("Error on reading Battery voltage");
  }

  Serial.println("-------------------------");
}

void lcdPrintInfo(){
  lcd.clear();  //clear the screen
  //------------Static Info--------------//
  lcd.setCursor(19,1);
  lcd.write(byte(0));

  //------------Dynamic Info------------//

  //Date-Time lcd{ (0,0)-(16,0)
  gprs.getDateTime(temp); //save Date-Time at temp variable | YY/MM/DD,HH:MM:SS+08
  lcd.setCursor(0,0); //set Cursor at top left corner
  lcd.print(temp[6]+temp[7]);
  lcd.print('/');
  lcd.print(temp[3]+temp[4]+temp[5]+temp[0]+temp[1]);
  //lcd.print(temp.substring(0,1));
  //lcd.print(temp.substring(8,16));
  //Date-Time

  //Signal Strength (19,0)
  lcd.setCursor(19,0);
  if(gprs.getSignalStrength(&signalStrength) == 1){
    if(signalStrength >=2 && signalStrength <=9){
      lcd.write(byte(1));
    }else if(signalStrength >=10 && signalStrength <=14){
      lcd.write(byte(2));
    }else if(signalStrength >=15 && signalStrength <=19){
      lcd.write(byte(3));
    }else if(signalStrength >=20 && signalStrength <=30){
      lcd.write(byte(4));
    }else{
      lcd.print('X');
    }
  }else{
    lcd.print('!');
  }
  //SignalStrength
  
}
/*void smsDiag(){
  gprs.sendSMS(phone,"network: "+net+"sim: "+sim+D+T+"signal: "+wordSig+valueSig);
  //dataSms=("network: "+net+"sim"+sim+D+T+"signal "+wordSig+valueSig);
  net= gprs.isNetworkRegistered();
  sim= gprs.checkSIMStatus();
  if(gprs.getDateTime(temp) == 1){
    D=("Date-Time: ");
    T=(temp);
  }else{
    D=("Error on reading Date-Time");
  }
  if(gprs.getSignalStrength(&signalStrength) == 1){
    Serial.print("Signal strength:  ");
    if(signalStrength >=2 && signalStrength <=9){
    wordSig=("Marginal: ");
    valueSig=(signalStrength);}
    if(signalStrength >=10 && signalStrength <=14){
    wordSig=("OK: ");
    valueSig=(signalStrength);}
    if(signalStrength >=15 && signalStrength <=19){
    wordSig=("Good: ");
    valueSig=(signalStrength);}
    if(signalStrength >=20 && signalStrength <=30){
    wordSig=("Excellent: ");
    valueSig=(signalStrength);}
  }else{
    wordSig("Error on reading Signal Strength");
  } 
  
}*/
